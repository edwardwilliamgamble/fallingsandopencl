import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.PointerBuffer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opencl.*;
import org.lwjgl.opencl.api.Filter;
import org.lwjgl.opengl.*;
import org.lwjgl.util.Color;
import org.lwjgl.util.ReadableColor;
import org.lwjgl.util.Timer;
import org.lwjgl.util.mapped.CacheUtil;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.FloatBuffer;
import java.nio.CharBuffer;
import java.nio.ShortBuffer;
import java.nio.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Random;

import static java.lang.Math.*;
import static org.lwjgl.opencl.CL10.*;
import static org.lwjgl.opencl.CL10GL.*;
import static org.lwjgl.opencl.KHRGLEvent.*;
import static org.lwjgl.opengl.AMDDebugOutput.*;
import static org.lwjgl.opengl.ARBCLEvent.*;
import static org.lwjgl.opengl.ARBDebugOutput.*;
import static org.lwjgl.opengl.ARBSync.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL21.*;

//import static org.lwjgl.opengl.GL30.*;

/**
 * Computes with OpenCL using multiple GPUs and renders the result with OpenGL.
 * A shared PBO is used as storage for the image.<br/>
 * <p>
 *
 */
public class Main {
	private Set<String> params;

	private static final boolean DEBUG = false;
	private boolean countElementsNow = false;

	private CLContext clContext;
	private CLCommandQueue queue;
	private CLKernel kernel;
	private CLProgram clProgram;

	private CLMem glBuffer;
	private IntBuffer glID;

	private boolean useTextures;

	// Texture rendering
	private int dlist;
	private int vsh;
	private int fsh;
	private int glProgram;

	private CLMem eleMap;

	private final PointerBuffer kernel2DGlobalWorkSize;
	private final PointerBuffer kernel2DLocalWorkSize;

        private Random random = new Random();
	private double frameRand = random.nextGaussian();

        private boolean vsync = false;
        private boolean fullscreen = false;

//	private static final int ELE_WIDTH = 10240;
//	private static final int ELE_HEIGHT = 5760;
//	private static final int ELE_WIDTH = 10000;
//	private static final int ELE_HEIGHT = 5000;
//	private static final int ELE_WIDTH = 5120;
//	private static final int ELE_HEIGHT = 2880;
	private static final int ELE_WIDTH = 1280;
	private static final int ELE_HEIGHT = 720;
//	private static final int ELE_WIDTH = 1920;
//	private static final int ELE_HEIGHT = 1080;
	private static final int ELE_MAP_SIZE = (ELE_WIDTH * ELE_HEIGHT);

	private int screenWidth = 1280;
	private int screenHeight = 720;
//	private int screenWidth = 1920;
//	private int screenHeight = 1080;
	private int screenCenterX = ELE_WIDTH/2;  //the pixel just left of center when screenWidth is even
	private int screenCenterY = ELE_HEIGHT/2;  //the pixel just below of center when screenHeight is even
	private int screenZoom = 1; //1 element = how many pixels wide on screen
	private int frameNum = 1;
	private int addDiameter = 0;
	private int addX = 0;
	private int addY = 0;
	private int addElem = 0;

	private boolean buffersInitialized;
	private boolean rebuild;

	private boolean run = true;

	private int totalCount = 0;
	private int airCount = 0;
	private int waterCount = 0;
	private int sandCount = 0;
	private int otherCount = 0;

	// EVENT SYNCING

	private final PointerBuffer syncBuffer = BufferUtils.createPointerBuffer(1);

	private boolean syncGLtoCL; // true if we can make GL wait on events generated from CL queues.
	private CLEvent clEvent;
	private GLSync clSync;

	private boolean syncCLtoGL; // true if we can make CL wait on sync objects generated from GL.
	private GLSync glSync;
	private CLEvent glEvent;

       private IntBuffer errorCode = BufferUtils.createIntBuffer(1);

	public Main(final String[] args) {
		params = new HashSet<String>();

		for ( int i = 0; i < args.length; i++ ) {
			final String arg = args[i];

			if ( arg.charAt(0) != '-' && arg.charAt(0) != '/' )
				throw new IllegalArgumentException("Invalid command-line argument: " + args[i]);

			final String param = arg.substring(1);

			if ( "forcePBO".equalsIgnoreCase(param) )
				params.add("forcePBO");
			else if ( "forceCPU".equalsIgnoreCase(param) )
				params.add("forceCPU");
			else if ( "debugGL".equalsIgnoreCase(param) )
				params.add("debugGL");
			else if ( "res".equalsIgnoreCase(param) ) {
				if ( args.length < i + 2 + 1 )
					throw new IllegalArgumentException("Invalid res argument specified.");

				try {
					this.screenWidth = Integer.parseInt(args[++i]);
					this.screenHeight = Integer.parseInt(args[++i]);

					if ( screenWidth < 1 || screenHeight < 1 )
						throw new IllegalArgumentException("Invalid res dimensions specified.");
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid res dimensions specified.");
				}
			}
		}

		kernel2DGlobalWorkSize = BufferUtils.createPointerBuffer(2);
		kernel2DLocalWorkSize = null; //let the drivers determine the best local work group sizes
	}

	public static void main(String args[]) {
		Main demo = new Main(args);
		demo.init();
		demo.run();
	}

/**
 * Set the display mode to be used 
 * 
 * @param width The width of the display required
 * @param height The height of the display required
 * @param fullscreen True if we want fullscreen mode
 */
public static boolean SetDisplayMode(int width, int height, boolean fullscreen) {

    // return if requested DisplayMode is already set
    if ((Display.getDisplayMode().getWidth() == width) && 
        (Display.getDisplayMode().getHeight() == height) && 
	(Display.isFullscreen() == fullscreen)) {
	    return true;
    }

    try {
        DisplayMode targetDisplayMode = null;
		
	if (fullscreen) {
	    DisplayMode[] modes = Display.getAvailableDisplayModes();
 	    int freq = 0;
				
	    for (int i=0;i<modes.length;i++) {
	        DisplayMode current = modes[i];
					
		if ((current.getWidth() == width) && (current.getHeight() == height)) {
		    if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
		        if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
			    targetDisplayMode = current;
			    freq = targetDisplayMode.getFrequency();
                        }
                    }

		    // if we've found a match for bpp and frequence against the 
		    // original display mode then it's probably best to go for this one
		    // since it's most likely compatible with the monitor
		    if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
                        (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
                            targetDisplayMode = current;
                            break;
                    }

                }

            }

        } else {
            targetDisplayMode = new DisplayMode(width,height);
        }

        if (targetDisplayMode == null) {
            System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
            return false;
        }

        Display.setDisplayMode(targetDisplayMode);
        Display.setFullscreen(fullscreen);
    } catch (LWJGLException e) {
        System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
        return false;
    }

    return true;
}

	public void init() {
		try {
			CL.create();

			if(!SetDisplayMode(screenWidth, screenHeight, fullscreen)){
				SetDisplayMode(screenWidth, screenHeight, false);
                        }

			Display.setTitle("Demo");
			Display.setSwapInterval(0);
			Display.create(new PixelFormat(), new ContextAttribs().withDebug(params.contains("debugGL")));
                        Display.setVSyncEnabled(vsync);
                        Keyboard.create();
                        Keyboard.enableRepeatEvents(false);
                        Mouse.create();
			Mouse.setGrabbed(false);
			Mouse.setClipMouseCoordinatesToWindow(true);
		} catch (LWJGLException e) {
			throw new RuntimeException(e);
		}

		try {
			initCL(Display.getDrawable());
		} catch (Exception e) {

			if ( clContext != null ){
				int res = clReleaseContext(clContext);

				if(CL_SUCCESS != res){
					System.out.println("ERROR: Could not release OpenCL context after LWJGL exception in init.");
				}

			}

			Display.destroy();
			throw new RuntimeException(e);
		}

		glDisable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		initView(Display.getDisplayMode().getWidth(), Display.getDisplayMode().getHeight());

		initGLObjects();
		glFinish();

		setKernelConstants();
	}

	private void initCL(Drawable drawable) throws Exception {
		// Find a platform
		List<CLPlatform> platforms = CLPlatform.getPlatforms();
		if ( platforms == null )
			throw new RuntimeException("No OpenCL platforms found.");

		final CLPlatform platform = platforms.get(0); // just grab the first one
		if ( platform == null )
			throw new RuntimeException("No OpenCL platforms found.");

		// Find devices with GL sharing support
		final Filter<CLDevice> glSharingFilter = new Filter<CLDevice>() {
			public boolean accept(final CLDevice device) {
				final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);
				return caps.CL_KHR_gl_sharing;
			}
		};
		int device_type = params.contains("forceCPU") ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU;
		List<CLDevice> devices = platform.getDevices(device_type, glSharingFilter);
		if ( devices == null ) {
			device_type = CL_DEVICE_TYPE_CPU;
			devices = platform.getDevices(device_type, glSharingFilter);
			if ( devices == null )
				throw new RuntimeException("No OpenCL devices found with KHR_gl_sharing support.");
		}

                CLDevice device = devices.get(0);

		// Create the context
		clContext = CLContext.create(platform, devices, new CLContextCallback() {
			protected void handleMessage(final String errinfo, final ByteBuffer private_info) {
				System.out.println("[CONTEXT MESSAGE] " + errinfo);
			}
		}, drawable, errorCode);
		checkCLError(errorCode, "CLContext.create in initCL");

		eleMap = clCreateBuffer(clContext, CL_MEM_READ_ONLY, ELE_MAP_SIZE, errorCode);
		checkCLError(errorCode, "clCreateBuffer in initCL");
		eleMap.checkValid();

		// create command queue and upload map buffers on each used device
		queue = clCreateCommandQueue(clContext, device, CL_QUEUE_PROFILING_ENABLE, errorCode);
		checkCLError(errorCode, "clCreateCommandQueue in initCL");
		queue.checkValid();

////		final ByteBuffer eleMapHostByteBuffer = BufferUtils.createByteBuffer(ELE_MAP_SIZE);
//		final ByteBuffer eleMapHostByteBuffer = CacheUtil.createByteBuffer(ELE_MAP_SIZE*2);
//ShortBuffer sb = eleMapHostByteBuffer.asShortBuffer();
//final ByteBuffer eleMapHostByteBuffer = ByteBuffer.allocateDirect(ELE_MAP_SIZE*2).order(ByteOrder.nativeOrder());



		final ByteBuffer eleMapHostByteBuffer = clEnqueueMapBuffer(queue, eleMap, CL_TRUE, CL_MAP_WRITE, 0, ELE_MAP_SIZE, null, null, errorCode);
		checkCLError(errorCode, "clEnqueueMapBuffer in initCL");



		initEleMap(eleMapHostByteBuffer);



		int res = clEnqueueUnmapMemObject(queue, eleMap, eleMapHostByteBuffer, null, null);
		checkCLError(res, "clEnqueueUnMapMemObject in initCL");




//		int res = clEnqueueWriteBuffer(queue, eleMap, CL_TRUE, 0, eleMapHostByteBuffer, null, null);
//		checkCLError(res, "clEnqueueWriteBuffer in initCL");

		// load program

		final ContextCapabilities caps = GLContext.getCapabilities();

		if ( !caps.OpenGL20 )
			throw new RuntimeException("OpenGL 2.0 is required to run this demo.");
		else if ( device_type == CL_DEVICE_TYPE_CPU && !caps.OpenGL21 )
			throw new RuntimeException("OpenGL 2.1 is required to run this demo.");

		if ( params.contains("debugGL") ) {
			if ( caps.GL_ARB_debug_output )
				glDebugMessageCallbackARB(new ARBDebugOutputCallback());
			else if ( caps.GL_AMD_debug_output )
				glDebugMessageCallbackAMD(new AMDDebugOutputCallback());
		}

		if ( device_type == CL_DEVICE_TYPE_GPU )
			System.out.println("OpenCL Device Type: GPU (Use -forceCPU to use CPU)");
		else
			System.out.println("OpenCL Device Type: CPU");
		for ( int i = 0; i < devices.size(); i++ )
			System.out.println("OpenCL Device #" + (i + 1) + " supports KHR_gl_event = " + CLCapabilities.getDeviceCapabilities(devices.get(i)).CL_KHR_gl_event);

		System.out.println("Display resolution: " + screenWidth + "x" + screenHeight + " (Use -res <width> <height> to change)");

		System.out.println("\nOpenGL caps.GL_ARB_sync = " + caps.GL_ARB_sync);
		System.out.println("OpenGL caps.GL_ARB_cl_event = " + caps.GL_ARB_cl_event);

		// Use PBO if we're on a CPU implementation
		useTextures = device_type == CL_DEVICE_TYPE_GPU && (!caps.OpenGL21 || !params.contains("forcePBO"));
		if ( useTextures ) {
			System.out.println("\nCL/GL Sharing method: TEXTURES (use -forcePBO to use PBO + DrawPixels)");
			System.out.println("Rendering method: Shader on a fullscreen quad");
		} else {
			System.out.println("\nCL/GL Sharing method: PIXEL BUFFER OBJECTS");
			System.out.println("Rendering method: DrawPixels");
		}

		buildClProgram();

		// Detect GLtoCL synchronization method
		syncGLtoCL = caps.GL_ARB_cl_event; // GL3.2 or ARB_sync implied

		if ( syncGLtoCL ) {
			System.out.println("\nGL to CL sync: Using OpenCL events");
		} else {
			System.out.println("\nGL to CL sync: Using clFinish");
		}

		// Detect CLtoGL synchronization method
		syncCLtoGL = caps.OpenGL32 || caps.GL_ARB_sync;

		if ( syncCLtoGL ) {

			if ( !CLCapabilities.getDeviceCapabilities(device).CL_KHR_gl_event ) {
				syncCLtoGL = false;
			}

		}

		if ( syncCLtoGL ) {
			System.out.println("CL to GL sync: Using OpenGL sync objects");
		} else {
			System.out.println("CL to GL sync: Using glFinish");
		}

		if ( useTextures ) {
			dlist = glGenLists(1);

			glNewList(dlist, GL_COMPILE);
			glBegin(GL_QUADS);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex2f(0, 0);

				glTexCoord2f(0.0f, 1.0f);
				glVertex2i(0, screenHeight);

				glTexCoord2f(1.0f, 1.0f);
				glVertex2f(screenWidth, screenHeight);

				glTexCoord2f(1.0f, 0.0f);
				glVertex2f(screenWidth, 0);
			}
			glEnd();
			glEndList();

			vsh = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vsh, getProgramSource("screen.vert"));
			glCompileShader(vsh);

			fsh = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fsh, getProgramSource("screen.frag"));
			glCompileShader(fsh);

			glProgram = glCreateProgram();
			glAttachShader(glProgram, vsh);
			glAttachShader(glProgram, fsh);
			glLinkProgram(glProgram);

			glUseProgram(glProgram);
			glUniform1i(glGetUniformLocation(glProgram, "progEntry"), 0);
		}

		System.out.println("");
	}

	private void buildClProgram() {
		/*
		 * workaround: The driver keeps using the old binaries for some reason.
		 * to solve this we simple create a new program and release the old.
		 * however rebuilding programs should be possible -> remove when drivers are fixed.
		 * (again: the spec is not very clear about this kind of usages)
		 */
		if ( clProgram != null ) {
			clReleaseProgram(clProgram);
		}

		try {
			createClProgram();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		final CLDevice device = queue.getCLDevice();

		final StringBuilder options = new StringBuilder(useTextures ? "-D USE_TEXTURE" : "");
		final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);

//		if ( jitterMultiSampleRays ) {
//			options.append(" -D JITTER_MULTI_SAMPLE_RAYS");
//		}

		System.out.println("\nOpenCL COMPILER OPTIONS: " + options);

		try {
			clBuildProgram(clProgram, device, options, null);
			checkCLError(errorCode, "clBuildProgram in buildClProgram");
		} finally {
			System.out.println("BUILD LOG: " + clProgram.getBuildInfoString(device, CL_PROGRAM_BUILD_LOG));
		}

		rebuild = false;

		// init kernel with constants
		kernel = clCreateKernel(clProgram, "progEntry", errorCode);
		checkCLError(errorCode, "clCreateKernel in buildClProgram");
	}

	private void initGLObjects() {

		if ( glBuffer == null ) {
			glID = BufferUtils.createIntBuffer(1);
		} else {
			clReleaseMemObject(glBuffer);

			if ( useTextures )
				glDeleteTextures(glID);
			else
				glDeleteBuffers(glID);

		}

		if ( useTextures ) {
			glGenTextures(glID);

			// Init textures
			glBindTexture(GL_TEXTURE_2D, glID.get(0));
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenWidth, screenHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, (ByteBuffer)null);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glBuffer = clCreateFromGLTexture2D(clContext, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, glID.get(0), errorCode);
			checkCLError(errorCode, "clCreateFromGLTexture2D in initGLObjects");
			glBindTexture(GL_TEXTURE_2D, 0);
		} else {
			glGenBuffers(glID);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, glID.get(0));
			glBufferData(GL_PIXEL_UNPACK_BUFFER, screenWidth * screenHeight * 4, GL_STREAM_DRAW);
			glBuffer = clCreateFromGLBuffer(clContext, CL_MEM_WRITE_ONLY, glID.get(0), errorCode);
			checkCLError(errorCode, "clCreateFromGLBuffer in initGLObjects");
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		}

		buffersInitialized = true;
	}

	// init kernels with constants

	private void setKernelConstants() {
		kernel
			.setArg(14, glBuffer)
			.setArg(15, ELE_WIDTH)
			.setArg(16, ELE_HEIGHT)
			.setArg(17, eleMap)
		;
	}

	// rendering cycle

	private void run() {
                long endFrameCount = 200l;
		long frameCounter = 0l;
                Timer avgFpsTimer = new Timer();
                Timer frameTimer = new Timer();
		float frameDuration = 0.0f;
		float shortestFrameDuration = Float.MAX_VALUE;
		float longestFrameDuration = 0.0f;

		while ( run ) {
			
			if ( !Display.isVisible() )
				Thread.yield();
/*
			try{
				Thread.sleep(100);
			}
			catch(InterruptedException ie){
			}
*/
			handleIO();
			display();

			Display.update();
			if ( Display.isCloseRequested() )
				break;

			frameCounter++;
                        Timer.tick();
			frameDuration = frameTimer.getTime();

			if(frameDuration < shortestFrameDuration){
				shortestFrameDuration = frameDuration;
			}

			if(frameDuration > longestFrameDuration){
				longestFrameDuration = frameDuration;
			}

			frameTimer.reset();
			
			if(frameCounter == endFrameCount){
//				countElementsNow = true;
                                frameCounter = 0l;
				float duration = avgFpsTimer.getTime();
				System.out.println(endFrameCount + " frames in " + duration + " s");
				System.out.println("average frame time: " + (duration/endFrameCount) + "s = " + (endFrameCount/duration) + "fps");
				System.out.println("shortest frame time: " + shortestFrameDuration + "s = " + (1.0f/shortestFrameDuration) + "fps");
				System.out.println("longest frame time: " + longestFrameDuration + "s = " + (1.0f/longestFrameDuration) + "fps");
				System.out.println();
				shortestFrameDuration = Float.MAX_VALUE;
				longestFrameDuration = 0.0f;
				avgFpsTimer.reset();
			}

		}

		clReleaseContext(clContext);

		if ( useTextures ) {
			glDeleteProgram(glProgram);
			glDeleteShader(fsh);
			glDeleteShader(vsh);

			glDeleteLists(dlist, 1);
		}

		CL.destroy();
		Display.destroy();
	}

	public void display() {
		// TODO: Need to clean-up events, test when ARB_cl_events & KHR_gl_event are implemented.

		// make sure GL does not use our objects before we start computing
		if ( syncCLtoGL && glEvent != null ) {
			clEnqueueWaitForEvents(queue, glEvent);
		} else
			glFinish();

		if ( !buffersInitialized ) {
			initGLObjects();
			setKernelConstants();
		}

		if ( rebuild ) {
			buildClProgram();
			setKernelConstants();
		}

		compute();
		render();
	}

	// OpenCL

	private void computeEachScreenPixel() {
		int drawOutput = 1;
		int calcRows = 0;
		int calcCols = 0;

		kernel2DGlobalWorkSize.put(0, screenWidth).put(1, screenHeight);
		//kernel2DLocalWorkSize.put(0, 10).put(1, 10); //let the drivers determine the workgroup sizes

		// start computation
		kernel
			.setArg(0, screenWidth)
			.setArg(1, screenHeight)
			.setArg(2, screenCenterX)
			.setArg(3, screenCenterY)
			.setArg(4, screenZoom)
			.setArg(5, frameNum)
			.setArg(6, (float)frameRand)
			.setArg(7, addDiameter)
			.setArg(8, addX)
			.setArg(9, addY)
			.setArg(10, addElem)
			.setArg(11, drawOutput)
			.setArg(12, calcRows)
			.setArg(13, calcCols)
		;

		// acquire GL objects, and enqueue the kernel with a probe from the list
		int res = clEnqueueAcquireGLObjects(queue, glBuffer, null, null);
		checkCLError(res, "clEnqueueAcquireGLObjects in computeEachScreenPixel");

		res = clEnqueueNDRangeKernel(queue, kernel, 2,
		                       null,
		                       kernel2DGlobalWorkSize,
		                       kernel2DLocalWorkSize,
		                       null, null);
		checkCLError(res, "clEnqueueNDRangeKernel in computeEachScreenPixel");

		clEnqueueReleaseGLObjects(queue, glBuffer, null, syncGLtoCL ? syncBuffer : null);

		if ( syncGLtoCL ) {
			clEvent = queue.getCLEvent(syncBuffer.get(0));
			clSync = glCreateSyncFromCLeventARB(queue.getParent(), clEvent, 0);
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			res = clFinish(queue);
			checkCLError(res, "clFinish in computeEachScreenPixel");
		}

	}

	private void computeEachElement() {
		int drawOutput = 0;
		int calcRows = 1;
		int calcCols = 1;

		kernel2DGlobalWorkSize.put(0, ELE_WIDTH).put(1, ELE_HEIGHT);
		//kernel2DLocalWorkSize.put(0, 10).put(1, 10); //let the drivers determine the workgroup sizes

		// start computation
		kernel
			.setArg(0, screenWidth)
			.setArg(1, screenHeight)
			.setArg(2, screenCenterX)
			.setArg(3, screenCenterY)
			.setArg(4, screenZoom)
			.setArg(5, frameNum)
			.setArg(6, (float)frameRand)
			.setArg(7, addDiameter)
			.setArg(8, addX)
			.setArg(9, addY)
			.setArg(10, addElem)
			.setArg(11, drawOutput)
			.setArg(12, calcRows)
			.setArg(13, calcCols)
		;

		// acquire GL objects, and enqueue the kernel with a probe from the list
		int res = clEnqueueAcquireGLObjects(queue, glBuffer, null, null);
		checkCLError(res, "clEnqueueAcquireGLObjects in computeAddElementsAndWriteVisual");

		res = clEnqueueNDRangeKernel(queue, kernel, 2,
		                       null,
		                       kernel2DGlobalWorkSize,
		                       kernel2DLocalWorkSize,
		                       null, null);
		checkCLError(res, "clEnqueueNDRangeKernel in computeAddElementsAndWriteVisual");

		clEnqueueReleaseGLObjects(queue, glBuffer, null, syncGLtoCL ? syncBuffer : null);

		if ( syncGLtoCL ) {
			clEvent = queue.getCLEvent(syncBuffer.get(0));
			clSync = glCreateSyncFromCLeventARB(queue.getParent(), clEvent, 0);
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			res = clFinish(queue);
			checkCLError(res, "clFinish in computeAddElementsAndWriteVisual");
		}

	}

	private void computeAddElementsAndWriteVisual() {
		int drawOutput = 1;
		int calcRows = 0;
		int calcCols = 0;

		kernel2DGlobalWorkSize.put(0, ELE_WIDTH).put(1, ELE_HEIGHT);
		//kernel2DLocalWorkSize.put(0, 10).put(1, 10); //let the drivers determine the workgroup sizes

		// start computation
		kernel
			.setArg(0, screenWidth)
			.setArg(1, screenHeight)
			.setArg(2, screenCenterX)
			.setArg(3, screenCenterY)
			.setArg(4, screenZoom)
			.setArg(5, frameNum)
			.setArg(6, (float)frameRand)
			.setArg(7, addDiameter)
			.setArg(8, addX)
			.setArg(9, addY)
			.setArg(10, addElem)
			.setArg(11, drawOutput)
			.setArg(12, calcRows)
			.setArg(13, calcCols)
		;

		// acquire GL objects, and enqueue the kernel with a probe from the list
		int res = clEnqueueAcquireGLObjects(queue, glBuffer, null, null);
		checkCLError(res, "clEnqueueAcquireGLObjects in computeAddElementsAndWriteVisual");

		res = clEnqueueNDRangeKernel(queue, kernel, 2,
		                       null,
		                       kernel2DGlobalWorkSize,
		                       kernel2DLocalWorkSize,
		                       null, null);
		checkCLError(res, "clEnqueueNDRangeKernel in computeAddElementsAndWriteVisual");

		clEnqueueReleaseGLObjects(queue, glBuffer, null, syncGLtoCL ? syncBuffer : null);

		if ( syncGLtoCL ) {
			clEvent = queue.getCLEvent(syncBuffer.get(0));
			clSync = glCreateSyncFromCLeventARB(queue.getParent(), clEvent, 0);
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			res = clFinish(queue);
			checkCLError(res, "clFinish in computeAddElementsAndWriteVisual");
		}

	}

	private void computeDropRows() {
		int drawOutput = 0;
		int calcRows = 1;
		int calcCols = 0;

		kernel2DGlobalWorkSize.put(0, ELE_WIDTH).put(1, 1);
		//kernel2DLocalWorkSize.put(0, 10).put(1, 10); //let the drivers determine the workgroup sizes

		// start computation
		kernel
			.setArg(0, screenWidth)
			.setArg(1, screenHeight)
			.setArg(2, screenCenterX)
			.setArg(3, screenCenterY)
			.setArg(4, screenZoom)
			.setArg(5, frameNum)
			.setArg(6, (float)frameRand)
			.setArg(7, 0)
			.setArg(8, addX)
			.setArg(9, addY)
			.setArg(10, addElem)
			.setArg(11, drawOutput)
			.setArg(12, calcRows)
			.setArg(13, calcCols)
		;

		// acquire GL objects, and enqueue the kernel with a probe from the list
		int res = clEnqueueAcquireGLObjects(queue, glBuffer, null, null);
		checkCLError(res, "clEnqueueAcquireGLObjects in computeDropRows");

		res = clEnqueueNDRangeKernel(queue, kernel, 2,
		                       null,
		                       kernel2DGlobalWorkSize,
		                       kernel2DLocalWorkSize,
		                       null, null);
		checkCLError(res, "clEnqueueNDRangeKernel in computeDropRows");

		clEnqueueReleaseGLObjects(queue, glBuffer, null, syncGLtoCL ? syncBuffer : null);

		if ( syncGLtoCL ) {
			clEvent = queue.getCLEvent(syncBuffer.get(0));
			clSync = glCreateSyncFromCLeventARB(queue.getParent(), clEvent, 0);
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			res = clFinish(queue);
			checkCLError(res, "clFinish in computeDropRows");
		}

	}

	private void computeSwapCols() {
		int drawOutput = 0;
		int calcRows = 0;
		int calcCols = 1;

		kernel2DGlobalWorkSize.put(0, 1).put(1, ELE_HEIGHT);
		//kernel2DLocalWorkSize.put(0, 10).put(1, 10); //let the drivers determine the workgroup sizes

		// start computation
		kernel
			.setArg(0, screenWidth)
			.setArg(1, screenHeight)
			.setArg(2, screenCenterX)
			.setArg(3, screenCenterY)
			.setArg(4, screenZoom)
			.setArg(5, frameNum)
			.setArg(6, (float)frameRand)
			.setArg(7, addDiameter)
			.setArg(8, addX)
			.setArg(9, addY)
			.setArg(10, addElem)
			.setArg(11, drawOutput)
			.setArg(12, calcRows)
			.setArg(13, calcCols)
		;

		// acquire GL objects, and enqueue the kernel with a probe from the list
		// acquire GL objects, and enqueue the kernel with a probe from the list
		int res = clEnqueueAcquireGLObjects(queue, glBuffer, null, null);
		checkCLError(res, "clEnqueueAcquireGLObjects in computeSwapCols");

		res = clEnqueueNDRangeKernel(queue, kernel, 2,
		                       null,
		                       kernel2DGlobalWorkSize,
		                       kernel2DLocalWorkSize,
		                       null, null);
		checkCLError(res, "clEnqueueNDRangeKernel in computeSwapCols");

		clEnqueueReleaseGLObjects(queue, glBuffer, null, syncGLtoCL ? syncBuffer : null);

		if ( syncGLtoCL ) {
			clEvent = queue.getCLEvent(syncBuffer.get(0));
			clSync = glCreateSyncFromCLeventARB(queue.getParent(), clEvent, 0);
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			res = clFinish(queue);
			checkCLError(res, "clFinish in computeSwapCols");
		}

	}

	private void compute() {
		frameNum++;
		frameRand = random.nextGaussian();

		computeEachScreenPixel();
//		computeEachElement();
//		computeAddElementsAndWriteVisual();
		computeDropRows();
		computeSwapCols();

		if(DEBUG && countElementsNow){
			countElementsNow = false;

			ByteBuffer readBuffer = BufferUtils.createByteBuffer(ELE_MAP_SIZE);  //just set to null?
			int res = clEnqueueReadBuffer(queue, eleMap, CL_TRUE, 0, readBuffer, null, null);
			checkCLError(res, "elEnqueueReadBuffer in compute");
			evaluateElements(readBuffer);

			//eleMapHostByteBuffer.clear();
			//int res = clEnqueueReadBuffer(queue, eleMap, CL_TRUE, 0, eleMapHostByteBuffer, null, null); //cannot reuse
			//checkCLError(res, "elEnqueueReadBuffer(reused buffer version) in compute");
			//evaluateElements(eleMapHostIntBuffer);
		}

	}

	// OpenGL

	private void render() {
		glClear(GL_COLOR_BUFFER_BIT);

		if ( syncGLtoCL ) {
			glWaitSync(clSync, 0, 0);
		}

                //draw output

		if ( useTextures ) {
			glBindTexture(GL_TEXTURE_2D, glID.get(0));
			glCallList(dlist);
			glBindTexture(GL_TEXTURE_2D, 0);
		} else {
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, glID.get(0));
			glRasterPos2i(screenWidth, 0);
			glDrawPixels(screenWidth, screenHeight, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		}

		if ( syncCLtoGL ) {
			glSync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
			glEvent = clCreateEventFromGLsyncKHR(clContext, glSync, null);
		}

	}

	private void handleIO() {
                Display.processMessages(); //gets latest Mouse (and Keyboard?) events from OS
                Mouse.poll();
                Keyboard.poll();

		addDiameter = 0;
		addX = 0;
		addY = 0;
		addElem = 0;

                final double dx = Mouse.getDX()*1.0;
                final double dy = Mouse.getDY()*1.0;
		final double dwheel = Mouse.getDWheel()*0.005;

		while ( Keyboard.next() ) {
			if ( Keyboard.getEventKeyState() )
				continue;

			final int key = Keyboard.getEventKey();


			if ( Keyboard.KEY_1 <= key && key <= Keyboard.KEY_9 ) {
				//int number = key - Keyboard.KEY_1 + 1;
			} else {
				switch ( Keyboard.getEventKey() ) {
					case Keyboard.KEY_ESCAPE:
						run = false;
						break;
					case Keyboard.KEY_0:
						screenZoom = 1;
						break;
					case Keyboard.KEY_MINUS:
						screenZoom -= 1;
						if(screenZoom == 0||screenZoom == -1) screenZoom = -2;
						if(screenZoom < -8) screenZoom = -8;
						break;
					case Keyboard.KEY_EQUALS:
						screenZoom += 1;
						if(screenZoom == 0||screenZoom == -1) screenZoom = 1;
						if(screenZoom > 8) screenZoom = 8;
						break;
				}
			}
		}

		if( Keyboard.isKeyDown(Keyboard.KEY_UP)){
			if(screenZoom < 0) screenCenterY += -screenZoom;
			else screenCenterY++;

			if(screenCenterY >= ELE_HEIGHT) screenCenterY = ELE_HEIGHT-1;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
			if(screenZoom < 0) screenCenterY -= -screenZoom;
			else screenCenterY--;

			if(screenCenterY < 0) screenCenterY = 0;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			if(screenZoom < 0) screenCenterX -= -screenZoom;
			else screenCenterX--;

			if(screenCenterX < 0) screenCenterX = 0;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			if(screenZoom < 0) screenCenterX += -screenZoom;
			else screenCenterX++;

			if(screenCenterX >= ELE_WIDTH) screenCenterX = ELE_WIDTH-1;
                }

		int eleScreenWidth = 0;
		int eleScreenHeight = 0;

		if(screenZoom < 0){
			eleScreenWidth = screenWidth*(-screenZoom);
			eleScreenHeight = screenHeight*(-screenZoom);
		}
		else{
			eleScreenWidth = screenWidth/(screenZoom);
			eleScreenHeight = screenHeight/(screenZoom);
		}

		int screenLX = screenCenterX-(eleScreenWidth/2);
		int screenLY = screenCenterY-(eleScreenHeight/2);
		//int screenUX = screenLX + eleScreenWidth-1;
		//int screenUY = screenLY + eleScreenHeight-1;

		int mouseWorldCoordX = 0;
		int mouseWorldCoordY = 0;

		if(screenZoom < 0){
			mouseWorldCoordX = screenLX + Mouse.getX()*-screenZoom;
			mouseWorldCoordY = screenLY + Mouse.getY()*-screenZoom;
		}
		else{
			mouseWorldCoordX = screenLX + Mouse.getX()/screenZoom;
			mouseWorldCoordY = screenLY + Mouse.getY()/screenZoom;
		}

		if(Mouse.isButtonDown(0)){
			addDiameter = 20;
			addX = mouseWorldCoordX;
			addY = mouseWorldCoordY;
			addElem = 2;
                }

		if(Mouse.isButtonDown(1)){
			addDiameter = 20;
			addX = mouseWorldCoordX;
			addY = mouseWorldCoordY;
			addElem = 1;
                }

		if(Mouse.isButtonDown(2)){
			addDiameter = 20;
			addX = mouseWorldCoordX;
			addY = mouseWorldCoordY;
			addElem = 3;
                }

		if(Mouse.isButtonDown(3)){
			addDiameter = 50;
			addX = mouseWorldCoordX;
			addY = mouseWorldCoordY;
			addElem = 2;
                }

	}

	private static boolean isDoubleFPAvailable(CLDevice device) {
		final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);
		return caps.CL_KHR_fp64 || caps.CL_AMD_fp64;
	}

	private void createClProgram() throws IOException {
		final String source = getProgramSource("prog.cl");
		clProgram = clCreateProgramWithSource(clContext, source, errorCode);
		checkCLError(errorCode, "clCreateProgramWithSource in createClProgram");
	}

	private String getProgramSource(final String file) throws IOException {
		InputStream source = null;
		URL sourceURL = Thread.currentThread().getContextClassLoader().getResource(file);
		if(sourceURL != null) {
			source = sourceURL.openStream();
		}
		if ( source == null ) // dev-mode
			source = new FileInputStream(file);
		final BufferedReader reader = new BufferedReader(new InputStreamReader(source));

		final StringBuilder sb = new StringBuilder();
		String line;
		try {
			while ( (line = reader.readLine()) != null )
				sb.append(line).append("\n");
		} finally {
			source.close();
		}

		return sb.toString();
	}


	private static void initView(int width, int height) {
		glViewport(0, 0, width, height);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, width, 0.0, height, 0.0, 1.0);
	}

	private void initEleMap(ByteBuffer byteBuff) {
		ByteBuffer buff = byteBuff;
		byte[] ba = new byte[ELE_HEIGHT*ELE_WIDTH];
//		ShortBuffer buff = byteBuff.asShortBuffer();
		int i = 0;
		int newTotalCount = 0;
		int newAirCount = 0;
		int newWaterCount = 0;
		int newSandCount = 0;
		int newOtherCount = 0;

                for(int y = 0; y < ELE_HEIGHT; y++){

	                for(int x = 0; x < ELE_WIDTH; x++){
				byte elem = 0;
				double r = random.nextGaussian();

				if(r > 0.66666){
					elem = 0;
				}
				else if(r > 0.33333){
					elem = 1;
				}
				else {
					elem = 2;
				}

				newTotalCount++;

				if(elem == 0)newAirCount++;
				else if(elem == 1)newWaterCount++;
				else if(elem == 2)newSandCount++;
				else newOtherCount++;
				ba[i] = elem;
//				buff.put((elem);
				i++;
			}

                }

		buff.put(ba);
		buff.flip();
		logNewCounts(newTotalCount, newAirCount, newWaterCount, newSandCount, newOtherCount);
	}

	private void evaluateElements(ByteBuffer byteBuff) {
		ByteBuffer buff = byteBuff;
//		ShortBuffer buff = byteBuff.asShortBuffer();
		int newTotalCount = 0;
		int newAirCount = 0;
		int newWaterCount = 0;
		int newSandCount = 0;
		int newOtherCount = 0;

		while(buff.hasRemaining()){
			byte elem = buff.get();
			newTotalCount++;

			if(elem == 0)newAirCount++;
			else if(elem == 1)newWaterCount++;
			else if(elem == 2)newSandCount++;
			else newOtherCount++;
		}

		logNewCounts(newTotalCount, newAirCount, newWaterCount, newSandCount, newOtherCount);
	}

	private void logNewCounts(int newTotalCount, int newAirCount, int newWaterCount, int newSandCount, int newOtherCount){
		int totalDiff = newTotalCount - totalCount;
		int airDiff = newAirCount - airCount;
		int waterDiff = newWaterCount - waterCount;
		int sandDiff = newSandCount - sandCount;
		int otherDiff = newOtherCount - otherCount;
		System.out.println();
		System.out.println("OLD Total: " + totalCount + ", Air: " + airCount + ", Water: " + waterCount + ", Sand: " + sandCount + ", Other: " + otherCount);
		System.out.println("NEW Total: " + newTotalCount + ", Air: " + newAirCount + ", Water: " + newWaterCount + ", Sand: " + newSandCount + ", Other: " + newOtherCount);
		System.out.println("Diff Total: " + totalDiff + ", Air: " + airDiff + ", Water: " + waterDiff + ", Sand: " + sandDiff + ", Other: " + otherDiff);
		System.out.println();
		totalCount = newTotalCount;
		airCount = newAirCount;
		waterCount = newWaterCount;
		sandCount = newSandCount;
		otherCount = newOtherCount;
	}

	private static void checkCLError(IntBuffer buffer, String id) {
		checkCLError(buffer.get(0), id);
	}

	private static void checkCLError(IntBuffer buffer) {
		checkCLError(buffer.get(0), "");
	}

	private static void checkCLError(int res, String id) {
		//org.lwjgl.opencl.Util.checkCLError(res);

		if(CL_SUCCESS != res){
			String errStr = "";

			switch(res){
				case CL_SUCCESS:
					errStr = "CL_SUCCESS";
					break;
				case CL_INVALID_COMMAND_QUEUE:
					errStr = "CL_INVALID_COMMAND_QUEUE";
					break;
				case CL_INVALID_CONTEXT:
					errStr = "CL_INVALID_CONTEXT";
					break;
				case CL_INVALID_MEM_OBJECT:
					errStr = "CL_INVALID_MEM_OBJECT";
					break;
				case CL_INVALID_VALUE:
					errStr = "CL_INVALID_VALUE";
					break;
				case CL_INVALID_EVENT_WAIT_LIST:
					errStr = "CL_INVALID_EVENT_WAIT_LIST";
					break;
				case CL_MEM_OBJECT_ALLOCATION_FAILURE:
					errStr = "CL_MEM_OBJECT_ALLOCATION_FAILURE";
					break;
				case CL_OUT_OF_HOST_MEMORY:
					errStr = "CL_OUT_OF_HOST_MEMORY";
					break;
				default:
					errStr = "UNKNOWN_ERROR";
					break;
			}

			System.out.println("ERROR: Error returned from OpenCL: " + errStr + " after: " + id);
			System.exit(0);
		}

	}


	private static void checkCLError(int res) {
		checkCLError(res, "");
	}

}

