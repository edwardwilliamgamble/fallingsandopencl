uniform sampler2D texSampler;
varying vec2 texCoord;

void main(void) {
	gl_FragColor = texture2D(texSampler, texCoord);
}