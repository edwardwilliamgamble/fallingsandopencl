/*
 * todo:
 * better random numbers
 *-different coloring and slightly different densities 
 *-need random column order? do not operate on the same elem that we draw, have to choose a random order on the x axis at least, bottom to top may be fine
 *-move water on the row level instead of just swapping individual pixels, keep track of segments of water and air and land
 *-simulate forces on each element and then move them accordingly (prob need to return to ints instead of bytes) to simulate water pressure for hydraulics
 *,also do not swap elements but instead allow air to compress to allow for movement space and add vacume space in the moved from pos if needed so it will be filled in next frame
 *-make left sloping and right sloping ?and flat sloped? sand that will affect how things stack/fall
 *-draw gui in opengl code in java over top of the texture written by this opencl code
 *-?draw elements in a fragment/pixel shader?
 */
#ifdef USE_TEXTURE
	#define OUTPUT_TYPE __write_only image2d_t
#else
	#define OUTPUT_TYPE global uint *
#endif

#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

float rand(float r){
	float discard;
	return fract ( r * r * 10246325.365253525237310, &discard );
}

//-128
//-127
//...
//-1
//+0
//+1
//...
//+126
//+127 anchored barrier


//1 moveable as opposed to anchored
//1 gas and therefore compressable as opposed to liquid or solid 
//0 solid as opposed to liquid or gas
//0 

//0
//0
//0
//0
kernel void progEntry(
	int screenWidth,
	int screenHeight,
	int screenCenterX,
	int screenCenterY,
	int screenZoom,
	int frameNum,
	float frameRand,
	int addDiameter,
	int addX,
	int addY,
	int addElem,//change to char?
	int drawOutput,
	int calcRows,   
	int calcCols,   
	OUTPUT_TYPE output,
	int eleWidth,
	int eleHeight,
	global char *ele
//	global short *ele
) {
	unsigned int ix = get_global_id(0);
	unsigned int iy = get_global_id(1);

	float discard;
	float rf = frameRand + fract(native_cos(1.0f*ix)*native_sin(1.0f*iy),&discard); //randoms change between pixels and from frame to frame
	rf = rand(rf);
	rf = rand(rf);

	if(drawOutput){

		if(ix == 0 && iy == 0 && addDiameter > 0 && addX >= 0 && addX < eleWidth && addY >= 0 && addY < eleHeight){

			for(int dy = addY - addDiameter/2; dy < eleHeight && dy < addY - addDiameter/2 + addDiameter; dy++){

				for(int dx = addX - addDiameter/2; dx < eleWidth && dx < addX - addDiameter/2 + addDiameter; dx++){

					if(dy >= 0 && dx >= 0){
						ele[eleWidth*(dy)+(dx)] = addElem;
					}

				}

			}

		}

		int eleScreenWidth = 0;
		int eleScreenHeight = 0;
		int screenLX = 0;
		int screenLY = 0;
		int ex = 0;
		int ey = 0;

		if(screenZoom < 1){
			eleScreenWidth = screenWidth*(-screenZoom);
			eleScreenHeight = screenHeight*(-screenZoom);
			screenLX = screenCenterX-(eleScreenWidth/2);
			screenLY = screenCenterY-(eleScreenHeight/2);
			ex = screenLX + ix*-screenZoom;
			ey = screenLY + iy*-screenZoom;
		}
		else{
			eleScreenWidth = screenWidth/(screenZoom);
			eleScreenHeight = screenHeight/(screenZoom);
			screenLX = screenCenterX-(eleScreenWidth/2);
			screenLY = screenCenterY-(eleScreenHeight/2);
			ex = screenLX + ix/screenZoom;
			ey = screenLY + iy/screenZoom;
		}

		float4 pixelColor;
		pixelColor = (float4)(0.3,0.3,0.3, 1.0);

                if(ex >= 0 && ex < eleWidth && ey >= 0 && ey < eleHeight){
		//draw prior state
			pixelColor = (float4)(0.0,0.0,0.0, 1.0);

			if(screenZoom < 0){

				for(int cy = 0; cy < -screenZoom; cy++){
					for(int cx = 0; cx < -screenZoom; cx++){
						char elem = ele[eleWidth*(ey+cy)+(ex+cx)];

						if(elem == -128) pixelColor += (float4)(1.0,0.0,1.0, 1.0);
						else if(elem == 0) pixelColor += (float4)(0.0,0.0,0.0, 1.0);
						else if(elem == 1) pixelColor += (float4)(0.0,0.2,1.0, 1.0);
						else if(elem == 2) pixelColor += (float4)(0.7,0.6,0.3, 1.0);
						else if(elem == 3) pixelColor += (float4)(1.0,0.0,0.0, 1.0);
						else if(elem == 127) pixelColor += (float4)(1.0,1.0,1.0, 1.0);
						else pixelColor += (float4)(0.0,1.0,0.0, 1.0);
					}
				}

				pixelColor /= (float4)(screenZoom*screenZoom+0.0,screenZoom*screenZoom+0.0,screenZoom*screenZoom+0.0, screenZoom*screenZoom+0.0);
				pixelColor.w = 1.0f;
			}
			else{
				char elem = ele[eleWidth*ey+ex];
				if(elem == -128) pixelColor = (float4)(1.0,0.0,1.0, 1.0);
				else if(elem == 0) pixelColor = (float4)(0.0,0.0,0.0, 1.0);
				else if(elem == 1) pixelColor = (float4)(0.0,0.2,1.0, 1.0);
				else if(elem == 2) pixelColor = (float4)(0.7,0.6,0.3, 1.0);
				else if(elem == 3) pixelColor = (float4)(1.0,0.0,0.0, 1.0);
				else if(elem == 127) pixelColor = (float4)(1.0,1.0,1.0, 1.0);
				else pixelColor = (float4)(0.0,1.0,0.0, 1.0);
			}

		}

		write_imagef(output, (int2)(ix,iy), (float4)pixelColor);
	}

	if(calcRows && calcCols){

	}

	if(calcCols&&!calcRows){
		int swapped = 0;
		int cx0 = 1;
		int cxinc = 2;
		int frmod = frameNum%4;

		if(frmod==0){
			cx0 = eleWidth;
			cxinc = -2;
		}
		else if(frmod==1){
			cx0 = 2;
			cxinc = 2;
		}
		else if(frmod==2){
			cx0 = eleWidth-1;
			cxinc = -2;
		}
		else if(frmod==3){
			cx0 = 1;
			cxinc = 2;
		}

		for(int cx = cx0; cx > 0 && cx < eleWidth; cx+=cxinc){
			char elem = ele[eleWidth*iy+cx];
			char elemLeft = ele[eleWidth*(iy)+(cx-1)];

			if(iy > 0 && (elem == 2 || elemLeft == 2) && elem < 3 && elemLeft < 3){
				char elemBelow = ele[eleWidth*(iy-1)+(cx)];
				char elemBelowLeft = ele[eleWidth*(iy-1)+(cx-1)];
				int willCurrLeftDrop = (elemLeft > elemBelowLeft);
				int willCurrThisDrop = (elem > elemBelow);
				int willNewLeftDropIfSwapped = (elem > elemBelowLeft);
				int willNewThisDropIfSwapped = (elemLeft > elemBelow);
				int currDrops = willCurrLeftDrop+willCurrThisDrop;
				int swapDrops = willNewLeftDropIfSwapped+willNewThisDropIfSwapped;

				if(swapDrops > currDrops){
					float chance = rf - floor(rf);
					rf = rand(rf);

					if(chance < 0.7){
						swapped = 1;
						ele[eleWidth*(iy)+(cx-1)] = elem;
						ele[eleWidth*(iy)+(cx)] = elemLeft;
					}

				}

			}

			if(!swapped){

				//water and air mixture do a swap if there is more water pressure on the water side then the air side
				if(elem < 2 && elemLeft < 2 && elem != elemLeft){
					int waterSidePressure = 0;
					int airSidePressure = 0;

					if(cx-2 >= 0){
						char elemTwoLeft = ele[eleWidth*(iy)+(cx-2)];

						if(elemTwoLeft == 1){

							if(elemLeft == 0){
								airSidePressure++;
							}
							else{
								waterSidePressure++;
							}

						}

					}

					if(cx+1 < eleWidth){
						char elemRight = ele[eleWidth*(iy)+(cx+1)];

						if(elemRight == 1){

							if(elem == 0){
								airSidePressure++;
							}
							else{
								waterSidePressure++;
							}

						}

					}

					if(waterSidePressure >= airSidePressure){
						float chance = rf - floor(rf);
						rf = rand(rf);

						if(chance < 0.7){
							ele[eleWidth*(iy)+(cx-1)] = elem;
							ele[eleWidth*(iy)+(cx)] = elemLeft;
						}

					}

				}


/*

				//water and air mixture do a random swap 50% of the time so that water levels out
				if(elem < 2 && elemLeft < 2 && elem != elemLeft){
					float swapProb = 0.5;
					char elemLeft = ele[eleWidth*(iy)+(cx-1)];

					float chance = rf - floor(rf);
					rf = rand(rf);

					if(chance < 0.5){
						ele[eleWidth*(iy)+(cx-1)] = elem;
						ele[eleWidth*(iy)+(cx)] = elemLeft;
					}

				}
*/
			}

		}

	}













	if(calcRows&&!calcCols){
		int cy = 1;
		int i = eleWidth*cy+ix;
		char temp = 0;
		global char * elemPtr = &ele[i];
		char elem = *elemPtr;
		global char * elemBelowPtr = &ele[i-eleWidth];
		char elemBelow = *elemBelowPtr;
		int swapped = 0;

		while(elemPtr < &ele[eleHeight*eleWidth]){
			swapped = 0;

			if(elem>elemBelow && elem < 3){
//				float prob = 1.0;

//				if(elemBelow == 1){
//					prob = 0.01;
//				}

//				float chance = rf - floor(rf);
//				rf = rand(rf);

//				if(chance < prob){
				//swap values
					temp = elemBelow;
					*elemBelowPtr = elem;
					*elemPtr = temp;
					swapped = 1;
//				}

			}

			if(swapped){
				//update comparison values
				elemBelowPtr = elemPtr;
				elemPtr += eleWidth;
				//elemBelow unchanged
				elem = *elemPtr;
			}
			else{
				//update comparison values
				elemBelowPtr = elemPtr;
				elemPtr += eleWidth;
				elemBelow = elem;
				elem = *elemPtr;
			}

		}

/*
		for(int cy = 1; cy < eleHeight; cy++){
			char elem = ele[eleWidth*cy+ix];
			char elemBelow = ele[eleWidth*(cy-1)+(ix)];

			//evaluate if this element is denser than the element below
			if(elem > elemBelow && elem<3){
//				float chance = rf - floor(rf);
//				rf = rand(rf);

//				if(chance < 0.9){
					ele[eleWidth*(cy-1)+(ix)] = elem;
					ele[eleWidth*(cy)+(ix)] = elemBelow;
//				}

			}

		}
*/
	}

}

